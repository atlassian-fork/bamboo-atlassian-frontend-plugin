package com.atlassian.bamboo.atlassianfrontend;

import com.atlassian.bamboo.commit.Commit;
import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.vcs.RepositoryChangeset;
import com.atlassian.bamboo.utils.HttpUtils;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.spotify.docker.client.shaded.javax.ws.rs.core.MediaType;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class AtlassianFrontendNotificationTransport implements NotificationTransport
{
    private static final Logger log = Logger.getLogger(AtlassianFrontendNotificationTransport.class);

    private final String afWebhookUrl;
    private final String afProduct;

    private CloseableHttpClient client;

    @Nullable
    private final ResultsSummary resultsSummary;

    public AtlassianFrontendNotificationTransport(String afWebhookUrl,
                                                  String afProduct,
                                                  @Nullable ResultsSummary resultsSummary,
                                                  CustomVariableContext customVariableContext)
    {
        this.afWebhookUrl = customVariableContext.substituteString(afWebhookUrl);
        this.afProduct = afProduct;
        this.resultsSummary = resultsSummary;

        URI uri;
        try
        {
            uri = new URI(this.afWebhookUrl);
        }
        catch (URISyntaxException e)
        {
            log.error("Unable to set up proxy settings, invalid URI encountered: " + e.getMessage(), e);
            return;
        }

        HttpUtils.EndpointSpec proxyForScheme = HttpUtils.getProxyForScheme(uri.getScheme());
        if (proxyForScheme!=null)
        {
            HttpHost proxy = new HttpHost(proxyForScheme.host, proxyForScheme.port);
            DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
            this.client = HttpClients.custom().setRoutePlanner(routePlanner).build();
        }
        else
        {
            this.client = HttpClients.createDefault();
        }
    }

    public void sendNotification(@NotNull Notification notification)
    {

        String message = (notification instanceof Notification.HtmlImContentProvidingNotification)
                ? ((Notification.HtmlImContentProvidingNotification) notification).getHtmlImContent()
                : notification.getIMContent();

        if (StringUtils.isNotEmpty(message) && resultsSummary != null)
        {
            try
            {
                HttpPost method = setupPostMethod();

                JSONObject object = new JSONObject();
                try {
                    JSONArray changesets = new JSONArray();

                    for (RepositoryChangeset changeset : resultsSummary.getRepositoryChangesets()) {
                        JSONObject changesetDetails = new JSONObject();
                        changesetDetails.put("id", changeset.getChangesetId());

                        JSONArray commits = new JSONArray();
                        for (Commit commit: changeset.getCommits()) {
                            commits.put(getJsonCommitData(commit));
                        }

                        changesetDetails.put("commits", commits);

                        changesets.put(changesetDetails);
                    }

                    object.put("changesets", changesets);

                    // The bamboo API has the typo
                    object.put("isSuccessful", resultsSummary.isSuccessful());
                    object.put("lifeCycleState", resultsSummary.getLifeCycleState().getValue());
                    object.put("buildTime", resultsSummary.getBuildTime());
                    object.put("buildNumber", resultsSummary.getBuildNumber());
                    object.put("planKey", resultsSummary.getPlanKey().getKey());
                    object.put("planResultKey", resultsSummary.getPlanResultKey().getKey());
                    object.put("shortReasonSummary", resultsSummary.getShortReasonSummary());
                    object.put("reasonSummary", resultsSummary.getReasonSummary());
                    object.put("product", afProduct);
                    
                    method.setEntity(new StringEntity(object.toString()));
                    method.setHeader("Accept", MediaType.APPLICATION_JSON);
                    method.setHeader("Content-type", MediaType.APPLICATION_JSON);
                } catch (JSONException e) {
                    log.error("JSON construction error :" + e.getMessage(), e);
                } catch (UnsupportedEncodingException e) {
                    log.error("Encoding error :" + e.getMessage(), e);
                }
                try {
                    log.debug(method.getURI().toString());

                    client.execute(method);
                } catch (IOException e) {
                    log.error("Error using Webhook API: " + e.getMessage(), e);
                }
            }
            catch(URISyntaxException e)
            {
                log.error("Error parsing webhook url: " + e.getMessage(), e);
            }
        }
    }

    private JSONObject getJsonCommitData(Commit commit) {
        JSONObject commitData = new JSONObject();

        try {
            commitData.put("message", commit.getComment());
            commitData.put("changeSetId", commit.getChangeSetId());
            commitData.put("authorEmail", commit.getAuthor().getEmail());
            commitData.put("date", commit.getDate().toInstant());
        } catch (JSONException e) {
            log.error("JSON construction error :" + e.getMessage(), e);
        }

        return commitData;
    }

    private HttpPost setupPostMethod() throws URISyntaxException
    {
        return new HttpPost(new URI(afWebhookUrl));
    }

}
